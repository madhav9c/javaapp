FROM alpine/git
WORKDIR /app
RUN git clone https://gitlab.com/ot-interview/javaapp.git

FROM maven:3.6.0-jdk-11-slim AS build
COPY --from=0 app/javaapp /app

RUN mvn -f /app clean install

FROM tomcat:8.0
COPY --from=1 /app/target/Spring3HibernateApp.war /usr/local/tomcat/webapps/
